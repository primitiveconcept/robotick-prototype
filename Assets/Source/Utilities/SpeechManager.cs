﻿using UnityEngine;
using System.Collections;

public class SpeechManager : MonoBehaviour 
{
	public TextMesh SpeechText;


	public void ReceiveSpeech(string speech)
	{
		SpeechText.text = speech;
	}
	
	
	public static void GetSpeech()
	{
		string intelligence = Managers.PlayerManager.Player1.Stats.Personality.DNA.ToString ();
		Application.ExternalCall ("GetSpeech", intelligence);
	}
}
