﻿using UnityEngine;
using System;
using System.Collections;

public class GameState : MonoBehaviour {

	public PlayerActor Player1;
	public GameObject GameOverScreen;
	public HUDController HudController;
	public static double HighScore = 0;
	public static Vector3[] BloodMap;
	public static Vector3[] HairMap;
	public static Vector3[] IngrownHairMap;
	public GameObject BloodPrefab;
	public GameObject HairPrefab;
	public GameObject IngrownHairPrefab;
	
	public static bool gameOver = true;
	public static float GameOverCountdown = 0f;


	public void NewGame()
	{
		Managers.PlayerManager.Player1.Stats = new PlayerStats();
		Managers.PlayerManager.Player1.Stats.ValueChanged += OnStatsUpdate;
		HudController.Start ();
		Player1.transform.position = Vector3.zero;
		Respawn();
	}
	
	public void GameOver()
	{
		gameOver = true;
		GameOverScreen.SetActive (true);
		GameOverCountdown = 3.0f;
	}
	
	public void Update()
	{
		if (gameOver && Input.GetKey(KeyCode.Space))
		{
				gameOver = false;
				GameOverScreen.SetActive(false);
				NewGame();
		}
	}

	public void Awake()
	{
		GameOverScreen.SetActive (true);
		SaveObjects();
	}
	
	public void Start()
	{
		Managers.PlayerManager.Player1.Stats.ValueChanged += OnStatsUpdate;
	}

	// Executes whenever player stats update.
	public void OnStatsUpdate(object sender, EventArgs eventArgs)
	{
		PlayerStats stats = (PlayerStats) sender;
		
		if (stats.Personality.DNA > HighScore)
			HighScore = stats.Personality.DNA;
		
		if (stats.Health <= 0)
		{
			GameOver();
		}
	}
	
	
	public void SaveObjects()
	{
		GameObject[] blood = GameObject.FindGameObjectsWithTag("BloodPool");
		GameObject[] hair = GameObject.FindGameObjectsWithTag("Hair");
		GameObject[] ingrownHair = GameObject.FindGameObjectsWithTag("IngrownHair");
		
		BloodMap = new Vector3[blood.Length];
		HairMap = new Vector3[hair.Length];
		IngrownHairMap = new Vector3[ingrownHair.Length];
		
		for (int i = 0; i < blood.Length; i++)
			BloodMap[i] = blood[i].transform.position;
		for (int i = 0; i < hair.Length; i++)
			HairMap[i] = hair[i].transform.position;
		for (int i = 0; i < ingrownHair.Length; i++)
			IngrownHairMap[i] = ingrownHair[i].transform.position;
	}
	
	
	public void Respawn()
	{
		GameObject[] blood = GameObject.FindGameObjectsWithTag("BloodPool");
		GameObject[] hair = GameObject.FindGameObjectsWithTag("Hair");
		GameObject[] ingrownHair = GameObject.FindGameObjectsWithTag("IngrownHair");
		
		for (int i = 0; i < blood.Length; i++)
			Destroy(blood[i]);
		for (int i = 0; i < hair.Length; i++)
			Destroy(hair[i]);
		for (int i = 0; i < ingrownHair.Length; i++)
			Destroy(ingrownHair[i]);

		for (int i = 0; i < BloodMap.Length; i++)
			Instantiate(BloodPrefab, BloodMap[i], Quaternion.identity);
		for (int i = 0; i < HairMap.Length; i++)
			Instantiate(HairPrefab, HairMap[i], Quaternion.identity);
		for (int i = 0; i < IngrownHairMap.Length; i++)
			Instantiate(IngrownHairPrefab, IngrownHairMap[i], Quaternion.identity);
	}
}
