﻿using UnityEngine;
using System;

[System.Serializable]
public class PlayerStats: ActorStats
{
	#region Base stats
	private IntelligenceStats _personality;
	public IntelligenceStats Personality 
	{ 
		get { return _personality; }
		set { _personality = value; OnValueChanged(); }
	}
	#endregion
	
	#region Derivatives
	public float Speed { get { return Personality.Joy / 5; } }
	public float Attack { get { return Personality.Aggression / 10; } }
	public float Defense { get { return Personality.Apathy / 10; } }
	public float Scavenging { get { return Personality.Logic / 10; } }
	#endregion
	
	#region Constructors
	public PlayerStats()
	{
		_personality = new IntelligenceStats();
	}
	#endregion
}
