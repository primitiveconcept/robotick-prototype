﻿using UnityEngine;
using System;
using System.Collections;

public class ActorStats 
{
	#region Constants
	public const float MAX_VALUE = 100;
	public const float MIN_VALUE = 0;
	#endregion
	
	public event EventHandler ValueChanged;
	
	#region Base stats
	[SerializeField] private float _health = 35;
	public float Health 
	{ 
		get { return _health; }
		set 
		{ 
			_health = Constrain(value); 
			OnValueChanged();
		}
	}
	#endregion
	
	// Notify any observers of changes.
	protected void OnValueChanged()
	{
		if (ValueChanged != null)
			ValueChanged(this, EventArgs.Empty);
	}
	
	
	private float Constrain(float value)
	{
		if (value > MAX_VALUE) value = MAX_VALUE;
		if (value < MIN_VALUE) value = MIN_VALUE;
		return value;
	}
}
