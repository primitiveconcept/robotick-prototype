﻿using System;

[System.Serializable]
public class IntelligenceStats
{
	#region Constants
	public const float MAX_VALUE = 100;
	public const float MIN_VALUE = 0;
	#endregion

	#region Fields
	private int _DNA = 0;
	private float _aggression;
	private float _apathy;
	private float _joy;
	private float _logic;
	#endregion
	
	#region Properties
	
	public float DNA 
	{
		get { return _DNA; }
		set { 
			if (value < 0f) value = 0f;
			_DNA = (int) value;
		}
	}
	
	// Can never actually reach x2.
	public float DNAMultiplier { get { return 1 + (float) (1 - Math.Log (2, (double) _DNA)); } }
	
	// Bite power up.
	public float Aggression
	{ 
		get { return _aggression * _DNA; }
		set { _aggression = Constrain(value); } 
	}
	
	// Defense up.
	public float Apathy 		
	{
		get { return _apathy * _DNA; }
		set { _apathy = Constrain(value); }
	}
	
	// Speed up.
	public float Joy
	{
		get { return _joy * _DNA; }
		set { _joy = Constrain(value); }
	}
	
	// DNA loss down.
	public float Logic 
	{
		get { return _logic * _DNA; }
		set { _logic = Constrain(value); }
	}
	#endregion
	
	
	private float Constrain(float value)
	{
		if (value > MAX_VALUE) value = MAX_VALUE;
		if (value < MIN_VALUE) value = MIN_VALUE;
		return value;
	}
}