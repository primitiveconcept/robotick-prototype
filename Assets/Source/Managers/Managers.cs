﻿using UnityEngine;
using System.Collections;

public class Managers : MonoBehaviour 
{
	#region Fields
	private static InputManager _inputManager;
	private static PlayerManager _playerManager;
	#endregion
	
	#region Properties
	
	public static InputManager InputManager
	{
		get { return _inputManager; }
	}
	
	public static PlayerManager PlayerManager
	{
		get { return _playerManager; }
	}
	
	#endregion
	
	
	public void Awake()
	{		
		_playerManager = new PlayerManager();
		_inputManager = new InputManager();
	}

	
}
