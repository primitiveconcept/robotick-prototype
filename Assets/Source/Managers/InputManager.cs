﻿using UnityEngine;
using System;
using System.Collections;

public class InputManager : IManager
{
	#region Properties
	public static Vector3 MousePosition
	{
		get { return Input.mousePosition; }
	}
	
	public static Vector3 MouseScreenPosition
	{
		get { return Camera.main.ScreenToWorldPoint(Input.mousePosition); }
	}
	#endregion

}