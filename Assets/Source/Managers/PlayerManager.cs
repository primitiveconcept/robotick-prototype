using UnityEngine;
using System;
using System.Collections;

public class PlayerManager : IManager
{
	public Player Player1 { get; set; }
	
	public PlayerManager()
	{
		Player1 = new Player();
	}
	
	
	public class Player
	{
		public PlayerStats Stats { get; set; }
		
		public Player()
		{
			Stats = new PlayerStats();
		}
	}
}
