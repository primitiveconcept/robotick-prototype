﻿using UnityEngine;
using System.Collections;

public class SortLayer : MonoBehaviour {
	public string SortingLayer = "GUI";

	// Use this for initialization
	void Start () {
		this.renderer.sortingLayerName = SortingLayer;
	}
}
