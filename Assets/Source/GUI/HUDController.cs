﻿using UnityEngine;
using System;
using System.Collections;

public class HUDController : MonoBehaviour {

	[SerializeField] private ProgressBar _healthMeter;
	[SerializeField] private TextMesh _dnaMeter;
	[SerializeField] private TextMesh _highScore;

	// Use this for initialization
	public void Start () 
	{
		// Watch for updates to player stats.
		Managers.PlayerManager.Player1.Stats.ValueChanged += OnStatsUpdate;
		
		_highScore.text = "Best: 0";
		_healthMeter.ProgressBarPercent = Managers.PlayerManager.Player1.Stats.Health;
		_dnaMeter.text = Managers.PlayerManager.Player1.Stats.Personality.DNA.ToString();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// Executes whenever player stats update.
	public void OnStatsUpdate(object sender, EventArgs eventArgs)
	{
		PlayerStats stats = (PlayerStats) sender;
				
		_healthMeter.ProgressBarPercent = stats.Health;
		_dnaMeter.text = stats.Personality.DNA.ToString();
		_highScore.text = "Best: " + GameState.HighScore.ToString();
	}
}
