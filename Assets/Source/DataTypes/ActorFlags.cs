﻿using System;

// Add flag: flags |= Flag.Enum
// Remove flag: flags &= Flag.Enum
// Check for flag: (flags & Flags.Enum) == Flag.Enum
[Flags]
public enum ActorFlags
{
	Attacking = 1 << 0,
	CanFeed = 1 << 1,
	Hampered = 1 << 2
}
