﻿using UnityEngine;
using System.Collections;


public class PlayerActor : Actor 
{
	public GameObject _BloodEmitter;
	public GameObject _StrikeEmitter;
	public GameObject _Emissions;

	public AudioClip attackSound;
	public AudioClip suckingSound;
	public AudioClip movingSound;
	
	private double _speechCountdown = 0;
	private double _speechTimer = 2;
	
	private bool _feeding = false;
	
	
	public override void Update()
	{
		// Bleed out.
		Managers.PlayerManager.Player1.Stats.Health -= Time.deltaTime * 3;
	
		if (_feeding)
			_speechCountdown += Time.deltaTime;
		
		
		// Left-click
		if (Input.GetMouseButton (0))
			HandleMovement();
		else
		{
			Movement.Stop ();
			if (audio.clip == movingSound)
				audio.Stop ();

			HandleIdle();
		}
		
		if (Input.GetMouseButton (1))
			HandleAttack();
		else
		{
			_BloodEmitter.SetActive(false);
			_StrikeEmitter.SetActive (false);
			_feeding = false;
		}
			
	}
	
	
	public override void HandleIdle ()
	{
		switch(Movement.Direction)
		{
		case Directions.Up:
			Animations.Play ("Up-Idle");
			break;
		case Directions.Down:
			Animations.Play ("Down-Idle");
			break;
		case Directions.Left:
			Animations.Play ("Left-Idle");
			break;
		case Directions.Right:
			Animations.Play ("Right-Idle");
			break;
		}	
	}
	
	
	public override void HandleMovement()
	{
		switch(Movement.Direction)
		{
			case Directions.Up:
				Animations.Play ("Up-Walk");
				_Emissions.transform.localPosition = new Vector3(0f, 0.8f, 0);
				break;
			case Directions.Down:
				Animations.Play ("Down-Walk");
				_Emissions.transform.localPosition = new Vector3(0f, -0.5f, 0);
				break;
			case Directions.Left:
				Animations.Play ("Left-Walk");
				_Emissions.transform.localPosition = new Vector3(-1f, 0.1f, 0);
				break;
			case Directions.Right:
				Animations.Play ("Right-Walk");
				_Emissions.transform.localPosition = new Vector3(1f, 0.1f, 0);
				break;
		}
		if (!audio.isPlaying)
		{
			audio.clip = movingSound;
			audio.Play();
		}

		Movement.MoveToward (InputManager.MouseScreenPosition);
	}
	
	
	public override void HandleAttack() 
	{
		if (HasFlag(ActorFlags.CanFeed))
		{
			Movement.Stop ();
			_BloodEmitter.SetActive (true);
			Managers.PlayerManager.Player1.Stats.Health += (10 * Time.deltaTime);
			Managers.PlayerManager.Player1.Stats.Personality.DNA += (1000 * Time.deltaTime);
			_feeding = true;
			HandleSpeech();

			if (!audio.isPlaying)
			{
				audio.clip = suckingSound;
				audio.Play();
			}
		}
		else
		{
			_feeding = false;
			_StrikeEmitter.SetActive (true);

			if (!audio.isPlaying)
			{
				audio.clip = attackSound;
				audio.Play();
			}
		}
		
		Attacking();
	}
	
	
	public void HandleSpeech()
	{
		if (_speechCountdown >= _speechTimer)
		{
			SpeechManager.GetSpeech ();
			Debug.Log ("Tick spoke!");
			_speechCountdown = 0;
		}
	}
}
