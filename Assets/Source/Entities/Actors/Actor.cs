using UnityEngine;
using System;


public class Actor: MonoBehaviour
{
	public GameObject _Sprite;
	
	public ActorFlags Flags { get; private set; }
	public Moveable Movement { get; private set; }
	public SpriteRenderer Renderer { get; private set; }
	public Animator Animations { get; private set; }
	
	public event EventHandler AttackObservers;
	
			
	public void Awake()
	{
		Movement = gameObject.GetComponent<Moveable>();
		Renderer = _Sprite.gameObject.GetComponent<SpriteRenderer>();
		Animations = _Sprite.gameObject.GetComponent<Animator>();
	}
	
	public virtual void Update()
	{
		// Left-click
		if (Input.GetMouseButton (0))
			HandleMovement();
		else
		{
			Movement.Stop ();
			HandleIdle();
		}
			
		if (Input.GetMouseButton (1))
			HandleAttack();
	}
	
	public virtual void HandleIdle() {}
	
	public virtual void HandleAttack() {}
	
	public virtual void HandleMovement()
	{
		if (!Movement.IsStopped)
			Movement.RotateToward (InputManager.MouseScreenPosition);
		
		if (Movement != null)
			Movement.MoveToward (InputManager.MouseScreenPosition);
	}
	
	protected void Attacking()
	{
		if (AttackObservers != null)
			AttackObservers(this, EventArgs.Empty);
	}
	
	
	public bool HasFlag(ActorFlags flag)
	{
		return ( (Flags & flag) == flag );
	}
	
	public void AddFlag(ActorFlags flag)
	{
		Flags |= flag;
	}
	
	public void RemoveFlag(ActorFlags flag)
	{
		Flags ^= flag;
	}
}


