﻿using UnityEngine;
using System;
using System.Collections;

public class Explosive: MonoBehaviour
{
	public float _ExplosionHealthDamage = 10;
	public float _ExplosionDNADamage = 10000;
	public GameObject _ExplosionEmitter;
	public Animator _animations;
	private SpriteFade _spriteFade;
	
	public void Awake()
	{
		_spriteFade = gameObject.GetComponent<SpriteFade>();
	}
	
	
	public void OnTriggerEnter2D(Collider2D trigger)
	{	
		if (trigger.gameObject.ImplementsInterface<PlayerActor>())
		{
			_ExplosionEmitter.SetActive (true);
			_animations.Play ("Explosion");
			audio.Play();
			Managers.PlayerManager.Player1.Stats.Health -= _ExplosionHealthDamage;
			Managers.PlayerManager.Player1.Stats.Personality.DNA -= _ExplosionDNADamage;
			_spriteFade.Fade (0, 1, delegate () { 
				Destroy(this.gameObject);
			});
		}
	}
}
