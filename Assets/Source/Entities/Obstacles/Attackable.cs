﻿using UnityEngine;
using System;
using System.Collections.Generic;


[RequireComponent(typeof(Collider2D))]
public class Attackable : MonoBehaviour {
	
	protected List<Actor> _attackers = new List<Actor>();
	
	
	public void OnTriggerEnter2D(Collider2D trigger)
	{
		Activate(trigger.gameObject);
	}
	
	public void OnTriggerExit2D(Collider2D trigger)
	{
		Deactivate(trigger.gameObject);
	}
	
	public void OnCollisionEnter2D(Collision2D trigger)
	{
		Activate(trigger.gameObject);
	}
	
	public void OnCollisionExit2D(Collision2D trigger)
	{
		Deactivate(trigger.gameObject);
	}
	
	
	public virtual void Activate(GameObject trigger)
	{
		if (trigger.ImplementsInterface<Actor>())
		{
			Actor actor = (Actor) trigger.gameObject.GetComponent<Actor>();
			if (!actor.HasFlag (ActorFlags.Attacking))
			{
				actor.AddFlag (ActorFlags.Attacking);
				actor.AttackObservers += Attacked;
				_attackers.Add (actor);
			}
		}
	}
	
	
	public virtual void Deactivate(GameObject trigger)
	{
		if (trigger.ImplementsInterface<Actor>())
		{
			Actor actor = (Actor) trigger.gameObject.GetComponent<Actor>();
			if (actor.HasFlag (ActorFlags.Attacking))
			{
				actor.RemoveFlag (ActorFlags.Attacking);
				actor.AttackObservers -= Attacked;
				_attackers.Remove (actor);
			}
		}
	}
	
	
	public virtual void Attacked(object attacker, EventArgs eventArgs) {}
	
	
	public virtual void DestroyedBy(Actor actor)
	{
		actor.RemoveFlag (ActorFlags.Attacking);
	}
		
		
	public virtual void OnDestroy()
	{
		foreach (Actor actor in _attackers)
		{
			_attackers.Remove (actor);
		}
	}
}
