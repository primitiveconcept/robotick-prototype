﻿using UnityEngine;
using System;
using System.Collections;


public class BloodSupply : Destroyable
{
	
	public override void Activate(GameObject trigger)
	{
		base.Deactivate(trigger);
	
		if (trigger.ImplementsInterface<Actor>())
		{
			Actor actor = (Actor) trigger.gameObject.GetComponent<Actor>();
			if (!actor.HasFlag (ActorFlags.CanFeed))
			{
				actor.AddFlag (ActorFlags.CanFeed);
				actor.AttackObservers += Attacked;
			}
		}
	}
	
	public override void Deactivate(GameObject trigger)
	{
		base.Deactivate(trigger);
		
		if (trigger.ImplementsInterface<Actor>())
		{
			
			Actor actor = (Actor) trigger.gameObject.GetComponent<Actor>();
			if (actor.HasFlag (ActorFlags.CanFeed))
			{
				actor.RemoveFlag (ActorFlags.CanFeed);
				actor.AttackObservers -= Attacked;
			}
		}
	}

		
	public override void DestroyedBy(Actor actor)
	{
		actor.RemoveFlag (ActorFlags.CanFeed);
		base.DestroyedBy (actor);
	}
	
	public override void OnDestroy()
	{
		foreach (Actor actor in _attackers)
			actor.RemoveFlag (ActorFlags.CanFeed);
		
		base.OnDestroy ();
	}
}
