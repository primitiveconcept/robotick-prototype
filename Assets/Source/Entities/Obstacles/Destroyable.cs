﻿using UnityEngine;
using System;
using System.Collections;


[RequireComponent(typeof(SpriteFade))]
public class Destroyable : Attackable
{
	public float _hitpoints = 10;
	public Animator _animations;
	private SpriteFade _spriteFade;
	
	
	public void Awake()
	{
		_spriteFade = gameObject.GetComponent<SpriteFade>();
	}
	
		
	public override void Attacked(object attacker, EventArgs eventArgs)
	{
		base.Attacked (attacker, eventArgs);
	
		_hitpoints -= Time.deltaTime * 10;
		
		// Destroy this gameobject
		if (_hitpoints < 0)
		{
			if (_animations != null)
				_animations.Play ("Destroyed");

			_spriteFade.Fade (0, 1, delegate () { 
				DestroyedBy((Actor) attacker);
				Destroy(this.gameObject);
			});
		}
		else 
		{
			if (_animations != null)
				_animations.Play ("Attacked");
		}
	}
}
