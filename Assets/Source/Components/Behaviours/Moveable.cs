﻿using UnityEngine;
using System;
using System.Collections;

public class Moveable : MonoBehaviour 
{
	#region Fields
	public float _MaxSpeed = 5f;
	public float _Acceleration = 0.5f;
	
	public Vector3 Position { get { return transform.position; } }
	private Vector3 _targetPosition;
	#endregion
	
	#region Properties
	public bool Moving { get; set; }
	public float AngularVelocity { get; set; }
	public float Velocity { get; set; }
	public float MaxSpeed { get{ return _MaxSpeed; } set{ _MaxSpeed = value; } }
	public bool IsStopped { get { return Position == _targetPosition || !Moving; } }
	#endregion
	
	
	public void Awake()
	{
		Moving = false;
		Velocity = 0f;
	}
	
	
	public void Update()
	{
		if (Moving)
			Move();
	}
	
	
	public void MoveToward(Vector3 position) 
	{
		_targetPosition = position;
		_targetPosition.z = Position.z;
		Moving = true;
	}
	
	
	private void Move()
	{		
		if (Position != _targetPosition)
		{
			if (Velocity < MaxSpeed)
				Velocity += _Acceleration;
			if (Velocity > MaxSpeed)
				Velocity = MaxSpeed; 
			
			transform.position = 
				Vector3.MoveTowards(Position, _targetPosition, Velocity * Time.deltaTime);
		}
		
		else Stop();
	}
	
	public void RotateToward(Vector3 position)
	{
		transform.rotation = 
			Quaternion.LookRotation(Vector3.forward, position - Position);
	}
	
	
	public void Stop()
	{
		Moving = false;
		Velocity = 0f;
	}
	
	
	public Directions Direction
	{
		get
		{
			float diffX = Position.x - _targetPosition.x;
			float diffY = Position.y - _targetPosition.y;
			
			// More horizontal
			if (Math.Abs (diffX) > Math.Abs (diffY))
			{
				if (diffX > 0)
					return Directions.Left;
				else
					return Directions.Right;
			}
			
			// More vertical
			else
			{
				if (diffY > 0)
					return Directions.Down;
				else
					return Directions.Up;
			}
		}
	}
}
