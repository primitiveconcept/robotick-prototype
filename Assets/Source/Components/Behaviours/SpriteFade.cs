﻿using UnityEngine;
using System;
using System.Collections;

public class SpriteFade : MonoBehaviour 
{
	public delegate void Callback();

	private float _fadeRate;
	private float _fadeCounter;
	private bool _fading = false;
	private float _newOpacity;
	private Callback _callback;
	
	private float _originalR;
	private float _originalG;
	private float _originalB;
	
	public SpriteRenderer _renderer;
	
	void Awake()
	{
		// _renderer = gameObject.GetComponent<SpriteRenderer>();
		
		_originalR = _renderer.color.r;
		_originalG = _renderer.color.g;
		_originalB = _renderer.color.b;
	}
	
	
	void Update () 
	{
		if (_fading)
		{
			_fadeCounter += Time.deltaTime * _fadeRate;
		
			float newAlpha = 0;
		
			if (_renderer.color.a < _newOpacity)
			{
				newAlpha = _renderer.color.a + _fadeCounter;
				if (newAlpha > _newOpacity)
					newAlpha = _newOpacity;
			}
			else if (_renderer.color.a > _newOpacity)
			{
				newAlpha = _renderer.color.a - _fadeCounter;
				if (newAlpha < _newOpacity)
					newAlpha = _newOpacity;
			}
				
			_renderer.color = new Color(_originalR, _originalG, _originalB, newAlpha);
			
			if (newAlpha == _newOpacity)
			{
				_fading = false;
				if (_callback != null)
					_callback();
			}
		}		
	}
	
	
	public void Fade(float newOpacity, float rate, Callback callback = null)
	{
		_fadeRate = rate;
		_newOpacity =  newOpacity / 100;
		_fadeCounter = 0;
		_fading = true;
		_callback = callback;
	}
}
