﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class SpriteEmitter : MonoBehaviour 
{
	public void Start()
	{
		particleSystem.renderer.sortingLayerID = 9;
	}
	
}
