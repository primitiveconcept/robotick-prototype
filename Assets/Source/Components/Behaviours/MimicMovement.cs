﻿using UnityEngine;
using System.Collections;

public class MimicMovement : MonoBehaviour {

	public GameObject TargetObject;
	
	public bool MatchRotation = false;
	public bool MatchPosition = true;
	
	
	[System.Serializable] public class Vector3Locks 
	{ public bool x, y, z = false; }
	public Vector3Locks PositionalLocks;
	
	public Quaternion RotationOffset = new Quaternion(0,0,0,0);
	public Vector3 PositionOffset = Vector3.zero;
	
	
	void Update () {
		if (MatchRotation)
		{
			transform.rotation = TargetObject.transform.rotation;
		}
		
		if (MatchPosition)
		{
			float newX = transform.position.x;
			float newY = transform.position.y;
			float newZ = transform.position.z;
			
			if (!PositionalLocks.x)
				newX = TargetObject.transform.position.x;
			
			if (!PositionalLocks.y)
				newY = TargetObject.transform.position.y;
				
			if (!PositionalLocks.z)
				newZ = TargetObject.transform.position.z;
				
			transform.position = new Vector3(newX, newY, newZ);
		}
	}
}
